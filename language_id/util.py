from collections import OrderedDict
from typing import List, Dict
import json

import torch
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

from ml.classifier.neural_network import NeuralNet


def get_ml_model(path: str) -> NeuralNet:
    weights: OrderedDict = torch.load(path)
    feature_count = weights['input_layer.weight'].shape[1]
    label_count = weights['output_layer.weight'].shape[0]

    model = NeuralNet(feature_count, label_count)
    model.load_state_dict(weights)

    return model


def get_languages(path: str) -> List[str]:
    languages = None
    with open(path, 'r') as file:
        content = file.read()
        languages = content.split()

    return languages


def get_vocabulary(path: str) -> Dict[str, int]:
    vocab = None
    with open(path, 'r') as file:
        vocab = json.load(file)

    return vocab


def convert_text_to_feature(text: str, vocab: Dict[str, int]) -> np.ndarray:
    vectorizer = CountVectorizer(
        analyzer='char', ngram_range=(3, 3), vocabulary=vocab
    )
    input_ = np.array([text])
    features = vectorizer.fit_transform(input_).toarray()

    return features
