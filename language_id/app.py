from typing import Dict, Optional

from fastapi import FastAPI, Query
import numpy as np
import torch

from language_id.util import (
    convert_text_to_feature,
    get_languages,
    get_ml_model,
    get_vocabulary,
)
from language_id.schema.language import DetectedLanguage, LanguageIdPayload
from ml.constants import LANG_CODE_NAME_MAP

print('loading ml model ...')
ml_model = get_ml_model('./resources/models/neural_net_classifier')
languages = get_languages('./resources/data/languages.txt')
vocabulary = get_vocabulary('./resources/data/vocabulary.json')

app = FastAPI()

POPULATE_QUERY_DESCRIPTION = 'If true, the response has probability of all the supported languages else the respone has the highest probability'
populate_query = Query(False, description=POPULATE_QUERY_DESCRIPTION)


@app.get('/health')
def health() -> Dict[str, str]:
    return {'hello': 'world'}


@app.post('/languages/identify')
def identify_language(
    payload: LanguageIdPayload, populate: Optional[bool] = populate_query
):
    text = payload.text
    feature = convert_text_to_feature(text, vocabulary).astype(np.float32)

    with torch.no_grad():
        tensor = torch.from_numpy(feature)
        predictions = ml_model(tensor).cpu().numpy()

    probabilities = list(predictions.reshape(-1))
    language_response = []

    for index, probability in enumerate(probabilities):
        language_iso = languages[index]
        language_name = LANG_CODE_NAME_MAP.get(language_iso, None)
        detected_language = DetectedLanguage(
            name=language_name,
            iso_code=language_iso,
            probability=round(probability, 2),
        )
        language_response.append(detected_language)

    sorted_language_response = sorted(
        language_response,
        key=lambda detected_language: detected_language.probability,
        reverse=True,
    )
    response = (
        sorted_language_response if populate else [sorted_language_response[0]]
    )

    return {'languages': response}
