from typing import List

from pydantic import BaseModel


class LanguageIdPayload (BaseModel):
    text: str

class DetectedLanguage (BaseModel):
    name: str
    iso_code: str
    probability: float

class LanguageIdResponse(BaseModel):
    languages: List[DetectedLanguage]
