# Langauge ID

## Development Environment Setup
1. Install [pipenv](https://pipenv.pypa.io/en/latest/), it is a package
   dependency resolver. Execute the command `pip install --user pipenv`.
2. `pipenv install` to install all the dependencies.
3. `pipenv shell` to activate virtual env created by pipenv.

### Test the model via REST API
1. `uvicorn language_id.app:app` to start the local server.

### For training ML model
Please make sure that you are in the root directory of the project, then
execute the following commands.

1. Execute `pythom -m ml.prepare_data resources/data/sentences.csv
   resources/data`, this would convert raw text data into ML model input
   features data and store it inside `resources` directory.
2. Execute `python -m ml.train resources/data resources/models/train
   resources/models` to train the model.

## API Schema
Once the server is up and running, the open API spec can be found at [/docs](http://localhost:8000/docs).
The raw schema is present in [open_api_schema.json](open_api_schema.json) file.


## Machine Learning
Please refer to [ml/README.md](ml/README.md) for details about ML model
implementation and approach.

## Time spent for completion
1. Going through some existing literature (mentioned in references): 1 hour 30 min
2. Setting up local development environment: 15 min.
3. Review `pandas` library: 30 min
4. Review `pytorch` library: 1 hour 30 min
5. Implementation of helper functions (`ml.prepare_data` and `ml.feature` modules) for transforming raw data to model
   injestable data: 2 hours 30 min (Structuring of code took time)
6. Implementation of training loop (`ml.classifer` module), and training script (`ml.train` module): 2 hours (had to
   change the implementation to account for lesser - 4 GiB VRAM - in
   local system)
7. Training of ML model: 1 hour
8. Exposing ML model using rest API: 25 min.
9. Documentation: 1 hour

## TODO
1. Use a logging library instead of using `print` function for training of ML
   model.
2. Use `multiprocessing` module to process raw data at lesser time.
3. Use git LFS for storing datasets for better developer experience.
