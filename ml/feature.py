from typing import Dict, List, Set, Tuple

import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import OneHotEncoder
from scipy.sparse.csr import csr_matrix

from ml.constants import LANGUAGE_COLUMN


class LanguageId:
    LANGUAGE_COLUMN = 'language'
    TEXT_COLUMN = 'text'

    def __init__(
        self,
        data: pd.DataFrame,
        languages: List[str],
        ngram_size: int = 3,
        max_features_per_language: int = 200,
    ) -> None:
        self._data = data
        self._languages = languages
        self._ngram_size = ngram_size
        self._max_features = max_features_per_language

        self.vocabulary = None

    def _get_ngrams(self, sentences: pd.Series) -> List[str]:
        """
        Return most frequent occuring ngrams from the given sentneces.
        """
        N = self._ngram_size
        vectorizer = CountVectorizer(
            analyzer='char',
            ngram_range=(N, N),
            max_features=self._max_features,
        )
        vectorizer.fit_transform(sentences)

        return vectorizer.get_feature_names_out().tolist()

    def _create_language_ngrams(self, language: str) -> List[str]:
        """
        Return all the ngrams for a given language.
        """
        lang_data: pd.DataFrame = self._data[
            self._data[self.LANGUAGE_COLUMN] == language
        ]
        lang_sentences: pd.Series = lang_data[self.TEXT_COLUMN]

        return self._get_ngrams(lang_sentences)

    def _create_vocabulary(self, ngrams: Set[str]) -> Dict[str, int]:
        """
        Return vocabulary from the given ngrams.
        """
        vocabulary: Dict[str, int] = {}

        for index, ngram in enumerate(ngrams):
            vocabulary[ngram] = index

        return vocabulary

    def create_features(
        self, vocabulary: Dict[str, int] = None
    ) -> Tuple[csr_matrix, np.ndarray]:
        print('creating features ...')
        N = self._ngram_size
        ngrams: Set[str] = set()
        sentences = self._data[self.TEXT_COLUMN]
        breakpoint()
        encoder = OneHotEncoder(sparse=False)
        lang_nd_array = self._data[self.LANGUAGE_COLUMN].values.reshape(-1, 1)
        labels = encoder.fit_transform(lang_nd_array)

        #  for each language compute it's ngrams
        for language in self._languages:
            lang_ngrams = self._create_language_ngrams(language)
            #  remove the rows having the language for which ngrams have been
            #  created
            self._data.drop(
                self._data[self._data[LANGUAGE_COLUMN] == language].index,
                inplace=True,
            )
            # update the n-gram corpus
            ngrams.update(lang_ngrams)

        #  create input feature matrix from sentences using the vocabulary
        if not vocabulary:
            vocabulary = self._create_vocabulary(ngrams)
            self.vocabulary = vocabulary
        else:
            self.vocabulary = vocabulary

        vectorizer = CountVectorizer(
            analyzer='char', ngram_range=(N, N), vocabulary=vocabulary
        )
        features: csr_matrix = vectorizer.fit_transform(sentences)
        # one hot encoding of languages

        print('feature creation completed ...')
        return features, labels
