import json
import math
import sys
from os.path import join
from typing import List, Tuple

import numpy as np
import pandas as pd
from scipy import sparse
from sklearn.model_selection import train_test_split

from ml.constants import (
    LANGUAGE_COLUMN,
    TEXT_COLUMN,
    COLUMNS,
    MIN_CHARS,
    MIN_SENTENCES,
    RANDOM_NUMBER_SEED,
)
from ml.feature import LanguageId


SplitData = Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]


def read_data(file_path: str) -> pd.DataFrame:
    """Read raw data and return it as a dataframe."""
    #  1st column is dropped
    data = pd.read_csv(
        file_path, sep='\t', header=None, usecols=[1, 2], names=COLUMNS
    )

    return data


def get_min_sentences_lang_list(
    df: pd.DataFrame, sentence_count: int
) -> List[str]:
    """Get languages that have minimum number of sentences."""
    lang_sentence_count: pd.DataFrame = df.groupby(by=LANGUAGE_COLUMN).count()
    matched_langs: pd.DataFrame = lang_sentence_count[
        lang_sentence_count[TEXT_COLUMN] >= sentence_count
    ]

    return list(matched_langs.index.values)


def process_data(
    data: pd.DataFrame,
    min_chars: int = MIN_CHARS,
    min_sentences: int = MIN_SENTENCES,
) -> Tuple[pd.DataFrame, List[str]]:
    """
    Take the raw data as input. Select sentences which have atleast provided
    number of minimum characters.
    Finally, select those languages which have atleast provided number of
    sentences.
    """
    print('processing data ...')
    min_char_filter = lambda string: len(string) >= min_chars

    #  select rows which have atleast 15 chars
    data_min_chars: pd.DataFrame = data[
        data[TEXT_COLUMN].apply(min_char_filter)
    ]
    #  get languages which have atleast `MIN_SENTENCES`
    lang_min_sentences: List[str] = get_min_sentences_lang_list(
        data_min_chars, min_sentences
    )
    #  data frame with sentences having atleast `MIN_CHARS` characters and
    #  languages having atleast `MIN_SENTENCES` sentences
    data_min_sentences: pd.DataFrame = data_min_chars.loc[
        data_min_chars[LANGUAGE_COLUMN].isin(lang_min_sentences)
    ]

    print('data processing completed ...')
    return data_min_sentences, lang_min_sentences


def train_val_test_split(
    data: pd.DataFrame,
    train: float = 0.7,
    validation: float = 0.2,
    test: float = 0.1,
    random_state=RANDOM_NUMBER_SEED,
) -> SplitData:
    """
    Split data into training, validation, and test datasets.
    """
    total = train + validation + test
    if math.ceil(total) != 1:
        raise Exception(
            '`train`, `validation`, and `test` values should add to 1.0'
        )
    train_ratio = train / total
    validation_and_test_ratio = (validation + test) / total

    #  split entire data into train, and test.
    #  test would be further split into test and validation
    train_data, val_and_test_data = train_test_split(
        data,
        train_size=train_ratio,
        random_state=random_state,
    )

    #  split val_and_test into validation and test
    validation_ratio = validation / validation_and_test_ratio
    validation_data, test_data = train_test_split(
        val_and_test_data,
        train_size=validation_ratio,
        random_state=random_state,
    )

    return train_data, validation_data, test_data


def create_train_val_test_data(
    processed_data: pd.DataFrame,
    languages: List[str],
    count: int = 2_00_000,
    random_state: int = RANDOM_NUMBER_SEED,
) -> SplitData:
    print('splitting data into training, validation, and test ...')
    train_sampled_language_texts = []
    validation_sampled_language_texts = []
    test_sampled_language_texts = []

    for language in languages:
        language_data = processed_data[
            processed_data[LANGUAGE_COLUMN] == language
        ]
        sampled_language_data = language_data.sample(
            n=count, random_state=random_state
        )
        train_df, validation_df, test_df = train_val_test_split(
            sampled_language_data
        )

        train_sampled_language_texts.append(train_df)
        validation_sampled_language_texts.append(validation_df)
        test_sampled_language_texts.append(test_df)

    train_data = pd.concat(train_sampled_language_texts).sample(
        frac=1, random_state=random_state * 2
    )
    train_data.reset_index(drop=True, inplace=True)
    validation_data = pd.concat(validation_sampled_language_texts)
    test_data = pd.concat(test_sampled_language_texts)

    print('data splitting completed ...')
    return train_data, validation_data, test_data


if __name__ == '__main__':
    _, raw_data_path, processed_data_directory = sys.argv

    # read raw data and process it
    data = read_data(raw_data_path)
    processed_data, languages = process_data(data)
    languages = sorted(languages)
    del data

    # store processed data and the languages
    processed_data_file_path = join(
        processed_data_directory, 'processed_data.csv'
    )
    processed_data.to_csv(processed_data_file_path, sep='\t')
    languages_data_file_path = join(processed_data_directory, 'languages.txt')
    with open(languages_data_file_path, 'w') as file:
        file.write('\n'.join(languages) + '\n')

    # from processed data create training, validation, and test dataframes
    # NOTE: dataframes still have sentences in raw text format
    train_df, validation_df, test_df = create_train_val_test_data(
        processed_data, languages, count=1_00_000
    )

    # convert raw text to features so that it can be used in models

    # training features
    print()
    print('training data ...')
    training_feature_extractor = LanguageId(train_df, languages)
    (
        training_input,
        training_labels,
    ) = training_feature_extractor.create_features()
    # store the vocabulary created while creating features for training data
    vocabulary = training_feature_extractor.vocabulary
    # save vocabulary
    vocabulary_file_path = join(processed_data_directory, 'vocabulary.json')
    with open(vocabulary_file_path, 'w') as file:
        json.dump(vocabulary, file)

    # using the vocabulary created in training data, the features of
    # validation and test data are obtained
    print()
    print('validation data ...')
    validation_input, validation_labels = LanguageId(
        validation_df, languages
    ).create_features(vocabulary=vocabulary)

    print()
    print('test data ...')
    test_input, test_labels = LanguageId(test_df, languages).create_features(
        vocabulary=vocabulary
    )

    #  store training, validation, and test input and labels
    train_input_path = join(processed_data_directory, 'training_input.npz')
    train_label_path = join(processed_data_directory, 'training_label.csv')
    sparse.save_npz(train_input_path, training_input)
    np.savetxt(train_label_path, training_labels, delimiter=',')

    validation_input_path = join(
        processed_data_directory, 'validation_input.npz'
    )
    validation_label_path = join(
        processed_data_directory, 'validation_label.csv'
    )
    sparse.save_npz(validation_input_path, validation_input)
    np.savetxt(validation_label_path, validation_labels, delimiter=',')

    test_input_path = join(processed_data_directory, 'test_input.npz')
    test_label_path = join(processed_data_directory, 'test_label.csv')
    sparse.save_npz(test_input_path, test_input)
    np.savetxt(test_label_path, test_labels, delimiter=',')
