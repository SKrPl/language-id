LANGUAGE_COLUMN = 'language'
TEXT_COLUMN = 'text'
COLUMNS = [LANGUAGE_COLUMN, TEXT_COLUMN]
MIN_CHARS = 15
MIN_SENTENCES = 2_00_000
RANDOM_NUMBER_SEED = 42

LANG_CODE_NAME_MAP = {
    'ber': 'berber',
    'deu': 'german',
    'eng': 'english',
    'epo': 'esperanto',
    'fra': 'french',
    'hun': 'hungarian',
    'ita': 'italian',
    'kab': 'kabyle',
    'por': 'portuguese',
    'rus': 'russian',
    'spa': 'spanish',
    'tur': 'turkish'
}
