from typing import Tuple, Callable, Type
from os.path import join

import numpy as np
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
from scipy.sparse.csr import csr_matrix

Optimizer = torch.optim.Optimizer


class LanguageIdData(Dataset):
    def __init__(self, input: csr_matrix, labels: np.ndarray) -> None:
        self._input = input
        self._labels = labels

    def __len__(self) -> int:
        return self._input.shape[0]

    def __getitem__(self, index) -> Tuple[torch.Tensor, torch.Tensor]:
        input = self._input[index].toarray().reshape(-1).astype(np.float32)
        #  label = self._labels[index].astype(np.float32)
        label = self._labels[index]

        return torch.from_numpy(input), torch.from_numpy(label)


class NeuralNet(nn.Module):
    def __init__(self, feature_count: int, label_count: int) -> None:
        super().__init__()
        self.input_layer = nn.Linear(feature_count, 512)
        nn.init.xavier_uniform_(self.input_layer.weight)
        nn.init.zeros_(self.input_layer.bias)

        self.hidden1 = nn.Linear(512, 256)
        nn.init.xavier_uniform_(self.hidden1.weight)
        nn.init.zeros_(self.hidden1.bias)

        self.hidden2 = nn.Linear(256, 64)
        nn.init.xavier_uniform_(self.hidden2.weight)
        nn.init.zeros_(self.hidden2.bias)

        self.output_layer = nn.Linear(64, label_count)
        nn.init.xavier_uniform_(self.output_layer.weight)
        nn.init.zeros_(self.output_layer.bias)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        out = torch.relu(self.input_layer(x))
        out = torch.relu(self.hidden1(out))
        out = torch.relu(self.hidden2(out))
        out = torch.softmax(self.output_layer(out), dim=1)

        return out


class Trainer:
    def __init__(
        self,
        model: nn.Module,
        loss_function: Callable,
        data: Dataset,
        validation_data: Dataset,
        test_data: Dataset,
        batch_size: int,
        device: str,
    ) -> None:
        self._model = model
        self._loss_function = loss_function

        self._data_loader = DataLoader(data, batch_size=batch_size)
        self._validation_data = validation_data
        self._test_data = test_data
        self._batch_size = batch_size
        self._device = device

        self._saved_model_paths = []
        self._best_model = None

    def _train_one_epoch(self, optimizer: Optimizer) -> float:
        running_loss: float = 0
        index = 0

        for batch_index, batch in enumerate(self._data_loader):
            x, y = batch

            optimizer.zero_grad()
            output = self._model(x.to(self._device))
            loss: torch.Tensor = self._loss_function(
                output, y.to(self._device)
            )
            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            index = batch_index

        return running_loss / index

    def train(
        self,
        optimizer: Optimizer,
        epochs: int,
        model_dir_path: str,
    ) -> None:
        print()
        print('training the model ...')

        self._model.train(True)
        for epoch in range(epochs):
            avg_loss = self._train_one_epoch(optimizer)
            print(f'epoch loss: {avg_loss}')

            path = join(model_dir_path, f'model_{epoch}')
            torch.save(self._model.state_dict(), path)
            self._saved_model_paths.append(path)

        print('training complete')

    def finalize_model_state(
        self,
        model_class: Type[NeuralNet],
        feature_count: int,
        label_count: int,
        model_dir_path: str,
    ) -> None:
        best_loss = 1e19

        print()
        print('selecting best model state ...')
        for model_path in self._saved_model_paths:
            print(f'computing validation loss for {model_path} ...')
            # load the saved model
            model = model_class(feature_count, label_count).to(self._device)
            model.load_state_dict(torch.load(model_path))
            model.eval()

            # compute the loss in validation data set
            index = 0
            running_loss = 0
            data_loader = DataLoader(
                self._validation_data, batch_size=self._batch_size
            )

            for batch_index, batch in enumerate(data_loader):
                index = batch_index
                x, y = batch

                with torch.no_grad():
                    output = model(x.to(self._device))
                    loss = self._loss_function(output, y.to(self._device))

                running_loss += loss.item()

            average_loss = running_loss / (index + 1)
            print(f'validation loss for {model_path} is: {average_loss}')
            print()

            if average_loss < best_loss:
                best_loss = average_loss
                self._best_model = model

        path = join(model_dir_path, 'neural_net_classifier')
        torch.save(self._best_model.state_dict(), path)

        print('best model state selected')

    def compute_accuracy(self) -> float:
        print()
        print('computing accuracy of the model ...')
        data_loader = DataLoader(self._test_data, batch_size=self._batch_size)
        correct_count = 0
        total = 0

        for _, batch in enumerate(data_loader):
            x, y = batch
            total += x.shape[0]

            with torch.no_grad():
                output: torch.Tensor = self._best_model(x.to(self._device))

            labels = torch.argmax(output, dim=1).cpu().numpy()
            y_ = torch.argmax(y, dim=1).cpu().numpy()
            correct_count += np.sum(labels == y_)

        accuracy = correct_count / total
        print(f'accuracy: {accuracy}')

        return accuracy
