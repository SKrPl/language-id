from os.path import join
import os
import sys

import numpy as np
import torch

from scipy.sparse import load_npz
from torch import nn

from ml.classifier.neural_network import LanguageIdData, NeuralNet, Trainer

if __name__ == '__main__':
    _, input_data_path, training_model_dir, final_model_dir = sys.argv

    # loading preprocessed data
    print('loading features ...')
    training_input_path = join(input_data_path, 'training_input.npz')
    training_label_path = join(input_data_path, 'training_label.csv')
    training_input = load_npz(training_input_path)
    training_labels = np.genfromtxt(training_label_path, delimiter=',')

    validation_input_path = join(input_data_path, 'validation_input.npz')
    validation_label_path = join(input_data_path, 'validation_label.csv')
    validation_input = load_npz(validation_input_path)
    validation_labels = np.genfromtxt(validation_label_path, delimiter=',')

    test_input_path = join(input_data_path, 'test_input.npz')
    test_label_path = join(input_data_path, 'test_label.csv')
    test_input = load_npz(test_input_path)
    test_labels = np.genfromtxt(test_label_path, delimiter=',')

    languages_path = join(input_data_path, 'languages.txt')
    languages = []
    with open(languages_path, 'r') as file:
        content = file.read()
        languages = content.split()

    print('feature loading completed')

    #  create pytorch training, validation, and test datasets
    training_data = LanguageIdData(training_input, training_labels)
    validation_data = LanguageIdData(validation_input, validation_labels)
    test_data = LanguageIdData(test_input, test_labels)

    #  create dependencies for training the neural network
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    network = NeuralNet(training_input.shape[1], len(languages)).to(device)
    learning_rate = 0.001
    optimizer = torch.optim.Adam(network.parameters(), lr=learning_rate)
    loss_function = nn.CrossEntropyLoss()

    trainer = Trainer(
        network,
        loss_function,
        training_data,
        validation_data,
        test_data,
        2048,
        device,
    )
    trainer.train(optimizer, 15, training_model_dir)
    #  model_paths = os.listdir(training_model_dir)
    #  trainer._saved_model_paths = [
    #      join(training_model_dir, path) for path in model_paths
    #  ]

    trainer.finalize_model_state(
        NeuralNet, training_input.shape[1], len(languages), final_model_dir
    )
    #  new_network = NeuralNet(training_input.shape[1], len(languages)).to(device)
    #  new_network.load_state_dict(
    #      torch.load(join(final_model_dir, 'neural_net_classifier'))
    #  )
    #  trainer._best_model = new_network
    trainer.compute_accuracy()
