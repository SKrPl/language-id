## Model Implementation

After going through some existing literatures (mentioned in References
section), and to implement it as a PoC, a simple vanilla neural network have
been used to identify different languages. For this, the data has been
downloaded from [Tatoeba](https://tatoeba.org/en/downloads) platform.

### Some Assumptions
Trigrams have been used for creating the corpus and converting raw text into input 
features for a neural network (more on this in `Why trigrams` section). The model has been trained with sentences which
have atleast 15 characters.

### Approach
First of all, those sentences are selected which have atleast 15 characters.
Then those languages are selected which have atleast 2 lakh sentences, this was
later reduced to 1 lakh due lack of availability of good GPU in local system.

Now, a machine learning model cannot understand string as input features, the
sentences need to be converted into vectors. The following approach has been
taken:
1. Convert each sentence as a list of trigrams.
2. Take 200 most common trigrams from each language.
3. Create a list of unique trigrams from each language's trigrams. This would
   be used as a vocabulary.
4. Create feature matrix, by counting the number of times each trigram occurs
   in each sentence.

### Supported Languages
The following languages are supproted:
* Berber (ber)
* German (deu)
* English (eng)
* Esperanto (epo)
* French (fra)
* Hungarian (hun)
* Italian (ita)
* Kabyle (kab)
* Portuguese (por)
* Russian (rus)
* Spanish (spa)
* Turkish (tur)

### Why trigrams?
If bigram or unigram is selected then it will capture a lot of redundant
information, and unnecessarily increase the size of the vocabulary. If ngrams
(where n > 3) is selected then it may work for some languages where words are
long enough to capture the features, but it won't work for languages where
words have lesser characters, we will get less features.
Hence trigrams are used.


### Why not classical ML models?
Tried to use SVM, the training was taking a lot of time (no logs were shown
related to progress), may be due to a large
training dataset (1322 features and ~16 lakh features).

For shorter sentences, Apple's ML model (mentioned in references) can be used
but it would take some time to understand bi-directional LSTM. So, stuck with
vanila neurla network.

### Accuracy
Accuracy of the model in test data is **96.59 %**.

## Resources
Intermediate and final results of computations are stored inside `resources`
directory present inside the project root directory. The following table
compiles the files/folders name and what do they store.

| Name | Location | Description |
| ---- | :------: | :---------: |
| sentences.csv | data | Stores the raw data |
| processed_data.csv | data | Stores sentences of languages which have minimum
number of provided characters and sentences |
| languages.txt | data | Stores the language IDs present in processed data |
| vocabulary.json | data | Stores the ngram vocabulary |
| training_input.npz | data | Sparse matrix which stores input features of training data |
| validation_input.npz | data | Sparse matrix which stores input features of validation data |
| test_input.npz | data | Sparse matrix which stores input features of test data |
| training_label.csv | data | Numpy array which stores labels of training data |
| validation_label.csv | data | Numpy array which stores labels of validation data |
| test_label.csv | data | Numpy array which stores labels of test data |
| models/train | models/train | Model states after each epoch during training |
| neural_net_classifer | models | Model state which is to be used in production |


## References
1. [Towards Data Science blog](https://towardsdatascience.com/deep-neural-network-language-identification-ae1c158f6a7d): Vanila neural network, this has been implemnted.
2. [Apple ML research blog](https://machinelearning.apple.com/research/language-identification-from-very-short-strings) : This is for very short strings, went through it, it is not implemented.
